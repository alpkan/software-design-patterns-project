﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Core.Mongo.Repository
{
    public interface IRepository<T> where T : class
    {
        Task<List<T>> FindAll();
        Task<T> FindOneAsync(Expression<Func<T, bool>> expression);
        Task<List<T>> FindManyAsync(Expression<Func<T, bool>> expression);
        List<T> FindMany(Expression<Func<T, bool>> expression);
        Task<object> CreateOneAsync(T document);
        List<object> CreateBatch(List<T> documents);
        Task<List<object>> CreateBatchAsync(List<T> documents);
        Task<long> UpdateOneAsync(Expression<Func<T, bool>> expression,
            params (Expression<Func<T, object>>, object)[] updatedProperties);
        long UpdateBatch(Expression<Func<T, bool>> expression,
            params (Expression<Func<T, object>>, object)[] updatedProperties);
        Task<long> ReplaceOneAsync(Expression<Func<T, bool>> expression, T document);
        Task<long> UpdateBatchAsync(Expression<Func<T, bool>> expression,
            params (Expression<Func<T, object>>, object)[] updatedProperties);
        long ReplaceBatch(Expression<Func<T, bool>> expression, List<T> documents);
        long DeleteBatch(Expression<Func<T, bool>> expression);
    }
}
