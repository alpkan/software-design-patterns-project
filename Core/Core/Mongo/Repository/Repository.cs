﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Core.Mongo.Context;
using MongoDB.Driver;

namespace Core.Mongo.Repository
{
    public abstract class Repository<T> : IRepository<T> where T : Document
    {
        private readonly IMongoCollection<T> _collection;

        protected Repository(IContext context, string collectionName = "")
        {
            if (string.IsNullOrEmpty(collectionName))
            {
                collectionName = typeof(T).Name;
            }
            collectionName = collectionName.First().ToString().ToLower() + collectionName.Substring(1);
            _collection = context.DbSet<T>(collectionName);
        }

        public Task<List<T>> FindAll()
        {
            var documents = _collection.AsQueryable().ToList();
            return Task.FromResult(documents);
        }

        public Task<T> FindOneAsync(Expression<Func<T, bool>> expression)
        {
            var document = _collection.AsQueryable().FirstOrDefault(expression);
            return Task.FromResult(document);
        }
        
        public async Task<List<T>> FindManyAsync(Expression<Func<T, bool>> expression)
        {
            var documents = await _collection.FindAsync(expression);
            return documents.ToList();
        }
        
        public List<T> FindMany(Expression<Func<T, bool>> expression)
        {
            var documents = _collection.Find(expression);
            return documents.ToList();
        }

        public async Task<object> CreateOneAsync(T document)
        {
            if (document.Id == null)
            {
                document.Id = Guid.NewGuid();
            }
            document.CreatedAt = DateTime.Now;
            document.UpdatedAt = DateTime.Now;
            await _collection.InsertOneAsync(document);
            return document.Id;
        }

        public async Task<List<object>> CreateBatchAsync(List<T> documents)
        {
            foreach (var document in documents)
            {
                if (document.Id == null)
                {
                    document.Id = Guid.NewGuid();
                }
                document.CreatedAt = DateTime.Now;
                document.UpdatedAt = DateTime.Now;
            }

            try
            {
                await _collection.InsertManyAsync(documents, new InsertManyOptions { IsOrdered = false});
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return documents.Select(d => d.Id).ToList();
        }

        public List<object> CreateBatch(List<T> documents)
        {
            foreach (var document in documents)
            {
                if (document.Id == null)
                {
                    document.Id = Guid.NewGuid();
                }
                document.CreatedAt = DateTime.Now;
                document.UpdatedAt = DateTime.Now;
            }

            try
            {
                _collection.InsertMany(documents, new InsertManyOptions { IsOrdered = false});
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return documents.Select(d => d.Id).ToList();
        }

        public async Task<long> UpdateOneAsync(Expression<Func<T, bool>> expression, params (Expression<Func<T, object>>, object)[] updatedProperties)
        {
            var update = Builders<T>.Update.Set(b => b.UpdatedAt, DateTime.Now);
            foreach (var (key, value) in updatedProperties)
            {
                update = update.Set(key, value);
            }
            var result = await _collection.UpdateOneAsync(expression, update);
            return result.ModifiedCount;
        }

        public async Task<long> UpdateBatchAsync(Expression<Func<T, bool>> expression, params (Expression<Func<T, object>>, object)[] updatedProperties)
        {
            var update = Builders<T>.Update.Set(b => b.UpdatedAt, DateTime.Now);
            foreach (var (key, value) in updatedProperties)
            {
                update = update.Set(key, value);
            }
            var result = await _collection.UpdateManyAsync(expression, update);
            return result.ModifiedCount;
        }

        public long UpdateBatch(Expression<Func<T, bool>> expression, params (Expression<Func<T, object>>, object)[] updatedProperties)
        {
            var update = Builders<T>.Update.Set(b => b.UpdatedAt, DateTime.Now);
            foreach (var (key, value) in updatedProperties)
            {
                update = update.Set(key, value);
            }
            var result = _collection.UpdateMany(expression, update);
            return result.ModifiedCount;
        }
        
        public async Task<long> ReplaceOneAsync(Expression<Func<T, bool>> expression, T document)
        {
            document.UpdatedAt = DateTime.Now;
            var result = await _collection.ReplaceOneAsync(expression, document);
            return result.ModifiedCount;
        }
        
        public long ReplaceBatch(Expression<Func<T, bool>> expression, List<T> documents)
        {
            var updates = new List<WriteModel<T>>();
            foreach (var doc in documents)
            {
                doc.UpdatedAt = DateTime.Now;
                updates.Add(new ReplaceOneModel<T>(expression, doc));
            }
            var result = _collection.BulkWrite(updates, new BulkWriteOptions() { IsOrdered = false });
            return result.ModifiedCount;
        }
        
        public long DeleteBatch(Expression<Func<T, bool>> expression)
        {
            var result = _collection.DeleteMany(expression);
            return result.DeletedCount;
        }
    }
}
