﻿using Core.Middlewares.Error;
using Microsoft.AspNetCore.Mvc;

namespace Core.Controller
{
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class CommonController : ControllerBase
    {
        [HttpGet("{*url}", Order = int.MaxValue)]
        public NotFound Index()
        {
            throw new NotFound(4040);
        }

        [HttpGet("/status")]
        public IActionResult Status()
        {
            return Ok(new { Status = "OK" });
        }
    }
}