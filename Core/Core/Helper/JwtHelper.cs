using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace Core.Helper
{
    public static class JwtHelper
    {
        private const string Secret = "9bc7efd5-af07-4896-9a97-5d894f28c838";

        public static string GenerateJwtToken(Guid id, string userType)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", id.ToString()), new Claim("type", userType) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
        
        public static TokenClaim ValidateJwtToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(Secret);
            try
            {
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;
                var id = new Guid(jwtToken.Claims.First(x => x.Type == "id").Value);
                var userType = jwtToken.Claims.First(x => x.Type == "type").Value;

                return new TokenClaim(id, userType);
            }
            catch
            {
                return null;
            }
        }
    }

    public class TokenClaim
    {
        public string UserType { get; set; }
        public Guid Id { get; set; }

        public TokenClaim(Guid id, string userType)
        {
            Id = id;
            UserType = userType;
        }
    }
}