namespace Core.Model.Response
{
    public class CheckUserIdResponseModel
    {
        public bool IsValid { get; set; }

        public CheckUserIdResponseModel(bool isValid)
        {
            IsValid = isValid;
        }
    }
}