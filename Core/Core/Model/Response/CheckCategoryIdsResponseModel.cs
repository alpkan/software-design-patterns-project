using System.Collections.Generic;

namespace Core.Model.Response
{
    public class CheckCategoryIdsResponseModel
    {
        public List<string> InvalidIds { get; set; }

        public CheckCategoryIdsResponseModel(List<string> invalidIds)
        {
            InvalidIds = invalidIds;
        }
    }
}