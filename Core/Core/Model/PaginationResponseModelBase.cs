namespace Core.Model
{
    public abstract class BaseListResponseModelBase<T>
    {
        public long TotalItemCount { get; set; }
        public T Data { get; set; }

        public BaseListResponseModelBase(T data, long totalItemCount)
        {
            TotalItemCount = totalItemCount;
            Data = data;
        }
    }
}