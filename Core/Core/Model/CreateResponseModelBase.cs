using Microsoft.AspNetCore.Mvc;

namespace Core.Model
{
    public abstract class CreateResponseModelBase
    {
        public string Location { get; set; }

        public CreateResponseModelBase(string location)
        {
            Location = location;
        }
    }
}