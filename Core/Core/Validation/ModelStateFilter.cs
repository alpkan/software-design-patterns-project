using System.Linq;
using Core.Middlewares.Error;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Core.Validation
{
    public class ModelStateFilter : IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                var (key, value) = context.ModelState.FirstOrDefault();
                throw new BadRequest(4000, key, value.AttemptedValue);
            }
        }
        public void OnActionExecuted(ActionExecutedContext context) { }
    }
}