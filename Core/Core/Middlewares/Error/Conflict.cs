namespace Core.Middlewares.Error
{
    public class Conflict : ErrorDetails
    {
        public Conflict(int code, string key = null, string variable = null, string message = null)
        {
            Code = code;
            StatusCode = 409;
            ErrorMessage = message ?? $"Already Exist";
        }
    }
}