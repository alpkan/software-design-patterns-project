namespace Core.Middlewares.Error
{
    public class Unauthorized : ErrorDetails
    {
        public Unauthorized(int code, string key = null, string variable = null, string message = null)
        {
            Code = code;
            StatusCode = 401;
            ErrorMessage = message ?? $"Unauthorized User";
        }
    }
}