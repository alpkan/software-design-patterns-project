﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UserService.Domain.RequestModel;
using UserService.Domain.ResponseModel;
using UserService.Exception;
using UserService.Service;

namespace UserService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IApplicationService _applicationService;

        public UserController(IApplicationService applicationService)
        {
            _applicationService = applicationService;
        }

        #region BackOffice/Admin
        
        /// <summary>
        /// Returns user
        /// </summary>
        /// <param name="id">Id of user</param>
        /// <returns>Returns user by given id.</returns>
        /// <response code="200">Returns user with id.</response>
        /// <response code="400">Id is not valid.</response>
        /// <response code="404">User not found.</response>
        /// <response code="500">Internal server error.</response>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(UserResponseModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Get([FromRoute] Guid id)
        {
            var (authUserId, authCheck) = _applicationService.CheckAuth(Request.Headers["Authorization"]);
            if (!authCheck)
            {
                throw new UserUnauthorized();
            }
            var user = await _applicationService.GetUserById(id);
            if (user == null)
            {
                throw new UserNotFound(id.ToString());
            }
            return Ok(user);
        }
        
        /// <summary>
        /// Create user.
        /// </summary>
        /// <param name="model">Id of user</param>
        /// <returns>Returns created user's id.</returns>
        /// <response code="201">The user was successfully created.</response>
        /// <response code="400">The model is not valid.</response>
        /// <response code="500">Internal server error.</response>
        [HttpPost]
        [Produces("application/json")]
        [ProducesResponseType(typeof(CreateUserRequestModel), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateAdmin([FromBody] CreateAdminRequestModel model)
        {
            var (authUserId, authCheck) = _applicationService.CheckAuth(Request.Headers["Authorization"]);
            if (!authCheck)
            {
                throw new UserUnauthorized();
            }
            var userId = await _applicationService.CreateUser(model.ToModel());
            return Ok(new CreateUserResponseModel(userId, !(userId == Guid.Empty)));
        }

        #endregion

    }
}