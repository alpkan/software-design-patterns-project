﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UserService.Domain.RequestModel;
using UserService.Domain.ResponseModel;
using UserService.Exception;
using UserService.Service;

namespace UserService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ReviewController : ControllerBase
    {
        private readonly IApplicationService _applicationService;
        
        public ReviewController(IApplicationService applicationService)
        {
            _applicationService = applicationService;
        }

        #region BackOffice/Admin
        

        /// <summary>
        /// Returns reviews
        /// </summary>
        /// <returns>Returns reviews.</returns>
        /// <response code="200">Returns review list.</response>
        /// <response code="500">Internal server error.</response>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ReviewListResponseModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Get([FromQuery] string status)
        {
            var (authUserId, authCheck) = _applicationService.CheckAuth(Request.Headers["Authorization"]);
            if (!authCheck)
            {
                throw new UserUnauthorized();
            }
            var reviews = await _applicationService.GetReviews(status);
            return Ok(new ReviewListResponseModel(reviews.Select(u => new ReviewResponseModel(u)).ToList(), reviews.Count));
        }
        
        /// <summary>
        /// Returns review
        /// </summary>
        /// <param name="id">Id of review</param>
        /// <returns>Returns review by given id.</returns>
        /// <response code="200">Returns review with id.</response>
        /// <response code="400">Id is not valid.</response>
        /// <response code="404">Review not found.</response>
        /// <response code="500">Internal server error..</response>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ReviewResponseModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Get([FromRoute] Guid id)
        {
            var (authUserId, authCheck) = _applicationService.CheckAuth(Request.Headers["Authorization"]);
            if (!authCheck)
            {
                throw new UserUnauthorized();
            }
            var review = await _applicationService.GetReviewById(id);
            if (review == null)
            {
                throw new ReviewNotFound(id.ToString());
            }
            return Ok(review);
        }

        /// <summary>
        /// Returns reviews
        /// </summary>
        /// <param name="id">Id of user</param>
        /// <returns>Returns reviews by given user id.</returns>
        /// <response code="200">Returns reviews with user id.</response>
        /// <response code="400">Id is not valid.</response>
        /// <response code="500">Internal server error..</response>
        [HttpPost("approve/{id}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ReviewResponseModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ApproveReview([FromRoute] Guid id)
        {
            var (authUserId, authCheck) = _applicationService.CheckAuth(Request.Headers["Authorization"]);
            if (!authCheck)
            {
                throw new UserUnauthorized();
            }
            await _applicationService.ApproveReview(id, authUserId);
            return Ok();
        }
        
        /// <summary>
        /// Returns reviews
        /// </summary>
        /// <param name="id">Id of user</param>
        /// <returns>Returns reviews by given user id.</returns>
        /// <response code="200">Returns reviews with user id.</response>
        /// <response code="400">Id is not valid.</response>
        /// <response code="500">Internal server error..</response>
        [HttpPost("reject/{id}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ReviewResponseModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> RejectReview([FromRoute] Guid id, [FromBody] RejectReviewRequestModel reason)
        {
            var (authUserId, authCheck) = _applicationService.CheckAuth(Request.Headers["Authorization"]);
            if (!authCheck)
            {
                throw new UserUnauthorized();
            }
            await _applicationService.RejectReview(id, authUserId, reason.Reason);
            return Ok();
        }

        #endregion

        #region Login User

        /// <summary>
        /// Returns reviews
        /// </summary>
        /// <returns>Returns reviews by given user id.</returns>
        /// <response code="200">Returns reviews with user id.</response>
        /// <response code="400">Id is not valid.</response>
        /// <response code="500">Internal server error..</response>
        [HttpGet("user")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ReviewResponseModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetUsersReview()
        {
            var tokenClaim = _applicationService.CheckUserAuth(Request.Headers["Authorization"]);
            if (tokenClaim == null)
            {
                throw new UserUnauthorized();
            }
            var reviews = await _applicationService.GetReviewsByUserId(tokenClaim.Id);
            return Ok(new ReviewListResponseModel(reviews.Select(u => new ReviewResponseModel(u)).ToList(), reviews.Count));
        }
        
        /// <summary>
        /// Create review.
        /// </summary>
        /// <param name="model">Review model</param>
        /// <returns>Returns created review's id.</returns>
        /// <response code="201">The review was successfully created.</response>
        /// <response code="400">The model is not valid.</response>
        /// <response code="500">Internal server error.</response>
        [HttpPost]
        [Produces("application/json")]
        [ProducesResponseType(typeof(CreateReviewResponseModel), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Post([FromBody] CreateReviewRequestModel model)
        {
            var tokenClaim = _applicationService.CheckUserAuth(Request.Headers["Authorization"]);
            if (tokenClaim == null)
            {
                throw new UserUnauthorized();
            }
            var reviewId = await _applicationService.CreateReview(model.ToModel(tokenClaim.Id));
            var path = $"{ControllerContext.HttpContext.Request.Path}/{reviewId}";
            return Ok(new CreateReviewResponseModel(reviewId, true, path));
        }
        
        /// <summary>
        /// Edit review.
        /// </summary>
        /// <param name="model">Update review model</param>
        /// <response code="200">The review was successfully edited.</response>
        /// <response code="400">The model is not valid.</response>
        /// <response code="404">Review not founc.</response>
        /// <response code="500">Internal server error.</response>
        [HttpPut("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Edit([FromRoute] Guid id, [FromBody] UpdateReviewRequestModel model)
        {
            var tokenClaim = _applicationService.CheckUserAuth(Request.Headers["Authorization"]);
            if (tokenClaim == null)
            {
                throw new UserUnauthorized();
            }
            var review = await _applicationService.EditReview(id, tokenClaim.Id, model.Content, model.Title, model.Star);
            return Accepted(review);
        }

        #endregion

    }
}