﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Core.Helper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UserService.Domain.RequestModel;
using UserService.Domain.ResponseModel;
using UserService.Exception;
using UserService.Service;

namespace UserService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PublicController : ControllerBase
    {
        private readonly IApplicationService _applicationService;
        
        public PublicController(IApplicationService applicationService)
        {
            _applicationService = applicationService;
        }

        /// <summary>
        /// Returns reviews
        /// </summary>
        /// <returns>Returns reviews.</returns>
        /// <response code="200">Returns review list.</response>
        /// <response code="500">Internal server error.</response>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ReviewListResponseModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetApprovedReviews()
        {
            var reviews = await _applicationService.GetReviews("approved");
            return Ok(new ReviewListResponseModel(reviews.Select(u => new ReviewResponseModel(u)).ToList(), reviews.Count));
        }
        
        /// <summary>
        /// Create user.
        /// </summary>
        /// <param name="model">Id of user</param>
        /// <returns>Returns created user's id.</returns>
        /// <response code="201">The user was successfully created.</response>
        /// <response code="400">The model is not valid.</response>
        /// <response code="500">Internal server error.</response>
        [HttpPost("register")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(CreateUserRequestModel), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Register([FromBody] CreateUserRequestModel model)
        {
            var userId = await _applicationService.CreateUser(model.ToModel());
            return Ok(new CreateUserResponseModel(userId, !(userId == Guid.Empty)));
        }

        /// <summary>
        /// Login.
        /// </summary>
        /// <param name="model">Id of user</param>
        /// <returns>Returns created user's id.</returns>
        /// <response code="200">The user was successfully created.</response>
        /// <response code="400">The model is not valid.</response>
        /// <response code="500">Internal server error.</response>
        [HttpPost("login")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(LoginRequestModel), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Login([FromBody] LoginRequestModel model)
        {
            var token = await _applicationService.Login(model.UserName, model.Password);
            return Ok(new TokenResponseModel(token));
        }
    }
}