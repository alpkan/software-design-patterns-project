using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Helper;
using UserService.Domain;

namespace UserService.Service
{
    public interface IApplicationService
    {
        Task<Review> GetReviewById(Guid id);
        Task<List<Review>> GetReviewsByUserId(Guid id);
        Task<List<Review>> GetReviews(string status);
        Task<Guid> CreateReview(Review review);
        Task<Review> EditReview(Guid id, Guid userId, string content, string title, int star);
        Task<User> GetUserById(Guid id);
        Task<List<User>> GetUsers();
        Task<Guid> CreateUser(User user);
        Task<string> Login(string username, string password);
        (Guid, bool) CheckAuth(string token);
        TokenClaim CheckUserAuth(string token);
        Task ApproveReview(Guid id, Guid operatedBy);
        Task RejectReview(Guid id, Guid operatedBy, string reason);
    }
}