using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Helper;
using Microsoft.Extensions.Logging;
using UserService.Domain;
using UserService.Exception;
using UserService.Repository;

namespace UserService.Service
{
    public class ApplicationService : IApplicationService
    {
        private readonly ILogger<ApplicationService> _logger;
        private readonly IUserRepository _userRepositoryDecorator;
        private readonly IReviewRepository _reviewRepository;
        
        public ApplicationService(ILogger<ApplicationService> logger, IUserRepository userRepositoryDecorator, IReviewRepository reviewRepository)
        {
            _logger = logger;
            _userRepositoryDecorator = userRepositoryDecorator;
            _reviewRepository = reviewRepository;
        }

        public async Task<Review> GetReviewById(Guid id)
        {
            return await _reviewRepository.GetReviewById(id);
        }

        public async Task<List<Review>> GetReviewsByUserId(Guid id)
        {
            var user = await _userRepositoryDecorator.GetUserById(id);
            if (user == null)
            {
                return new List<Review>();
            }
            return await _reviewRepository.GetReviewsByUserId(id);
        }

        public async Task<List<Review>> GetReviews(string status)
        {
            return await _reviewRepository.GetReviews(status);
        }
        
        public async Task<List<Review>> GetApprovedReviews()
        {
            return await _reviewRepository.GetReviews("approved");
        }

        public async Task<Guid> CreateReview(Review review)
        {
            return await _reviewRepository.CreateReview(review);
        }

        public async Task<Review> EditReview(Guid id, Guid userId, string content, string title, int star)
        {
            var review = await _reviewRepository.GetReviewById(id);
            if (userId != review.UserId)
            {
                throw new ReviewNotFound(id.ToString());
            }

            review.Star = star;
            review.Content = content;
            review.Title = title;
            review.Status = "pending";
            review.RejectReason = null;
            return await _reviewRepository.ReplaceReview(review);
        }

        public async Task<User> GetUserById(Guid id)
        {
            return await _userRepositoryDecorator.GetUserById(id);
        }

        public async Task<List<User>> GetUsers()
        {
            return await _userRepositoryDecorator.GetUsers();
        }

        public async Task<Guid> CreateUser(User user)
        {
            var exist = await _userRepositoryDecorator.GetUserByUsername(user.UserName);
            if (exist != null)
            {
                throw new AlreadyExists(user.UserName);
            }
            return await _userRepositoryDecorator.CreateUser(user);
        }

        public async Task<string> Login(string username, string password)
        { 
            var user = await _userRepositoryDecorator.GetUserByUsername(username);
            if (user == null || !BCrypt.Net.BCrypt.Verify(password, user.Password))
            {
                throw new UserNotFound();
            }
            return JwtHelper.GenerateJwtToken((Guid) user.Id, user.Type);
        }
        
        public (Guid, bool)  CheckAuth(string token)
        {
            if (token == null)
            {
                return (Guid.Empty, false);
            }
            token = token.Replace("Bearer ", "");
            var userClaim = JwtHelper.ValidateJwtToken(token);
            if (userClaim == null)
            {
                return (Guid.Empty, false);
            }
            return (userClaim.Id, userClaim.Id != Guid.Empty && userClaim.UserType == "admin");
        }
        
        public TokenClaim CheckUserAuth(string token)
        {
            if (token == null)
            {
                return null;
            }
            token = token.Replace("Bearer ", "");
            return JwtHelper.ValidateJwtToken(token);
        }

        public async Task ApproveReview(Guid id, Guid operatedBy)
        {
            var result = await _reviewRepository.ApproveReview(id, operatedBy);
            if (!result)
            {
                throw new ReviewNotFound(id.ToString());
            }
        }

        public async Task RejectReview(Guid id, Guid operatedBy, string reason)
        {
            var result = await _reviewRepository.RejectReview(id, operatedBy, reason);
            if (!result)
            {
                throw new ReviewNotFound(id.ToString());
            }
        }
    }
}