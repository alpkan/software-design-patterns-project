using System;
using Core.Model;

namespace UserService.Domain.ResponseModel
{
    public class CreateReviewResponseModel : CreateResponseModelBase
    {
        public Guid Id { get; set; }
        public bool Success { get; set; }

        public CreateReviewResponseModel(Guid id, bool success, string path) : base(path)
        {
            Id = id;
            Success = success;
        }
    }
}