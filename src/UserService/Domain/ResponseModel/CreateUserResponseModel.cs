using System;
using Core.Model;

namespace UserService.Domain.ResponseModel
{
    public class CreateUserResponseModel
    {
        public Guid Id { get; set; }
        public bool Success { get; set; }

        public CreateUserResponseModel(Guid id, bool success)
        {
            Id = id;
            Success = success;
        }
    }
}