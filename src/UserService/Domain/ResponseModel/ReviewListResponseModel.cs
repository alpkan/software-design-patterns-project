using System.Collections.Generic;
using Core.Model;

namespace UserService.Domain.ResponseModel
{
    public class ReviewListResponseModel : BaseListResponseModelBase<List<ReviewResponseModel>>
    {
        public ReviewListResponseModel(List<ReviewResponseModel> reviews, long totalItemCount) : base(reviews, totalItemCount)
        {
        }
    }
}