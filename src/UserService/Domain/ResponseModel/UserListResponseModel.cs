using System.Collections.Generic;
using Core.Model;

namespace UserService.Domain.ResponseModel
{
    public class UserListResponseModel : BaseListResponseModelBase<List<UserResponseModel>>
    {
        public UserListResponseModel(List<UserResponseModel> users, long totalItemCount) : base(users, totalItemCount)
        {
        }
    }
}