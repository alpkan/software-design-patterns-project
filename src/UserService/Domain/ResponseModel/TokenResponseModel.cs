using System;

namespace UserService.Domain.ResponseModel
{
    public class TokenResponseModel
    {
        public string Token { get; set; }

        public TokenResponseModel(string token)
        {
            Token = token;
        }
    }
}