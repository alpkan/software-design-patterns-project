using System;

namespace UserService.Domain.ResponseModel
{
    public class UserResponseModel
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string Email { get; set; }

        public UserResponseModel(User user)
        {
            Id = (Guid) user.Id;
            CreatedAt = user.CreatedAt;
            UpdatedAt = user.UpdatedAt;
            Email = user.Email;
        }
    }
}