using System;

namespace UserService.Domain.ResponseModel
{
    public class ReviewResponseModel
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string Content { get; set; }
        public string Title { get; set; }
        public int Star { get; set; }
        public string Status { get; set; }
        public string RejectReason { get; set; }
        public Guid OperatedBy { get; set; }

        public ReviewResponseModel(Review review)
        {
            Id = (Guid) review.Id;
            CreatedAt = review.CreatedAt;
            UpdatedAt = review.UpdatedAt;
            Content = review.Content;
            Title = review.Title;
            Star = review.Star;
            RejectReason = review.RejectReason;
            Status = review.Status;
            OperatedBy = review.OperatedBy;
        }
    }
}