using System;

namespace UserService.Domain.RequestModel
{
    public class RejectReviewRequestModel
    {
        public string Reason { get; set; }
    }
}