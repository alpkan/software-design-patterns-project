using System;

namespace UserService.Domain.RequestModel
{
    public class CreateReviewRequestModel
    {
        public string Content { get; set; }
        public string Title { get; set; }
        public int Star { get; set; }
        public Review ToModel(Guid userId)
        {
            return new Review
            {
                Content = Content,
                Title = Title,
                Star = Star,
                UserId = userId,
                Status = "pending"
            };
        }
    }
}