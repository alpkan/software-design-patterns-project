using System;

namespace UserService.Domain.RequestModel
{
    public class UpdateReviewRequestModel
    {
        public string Content { get; set; }
        public string Title { get; set; }
        public int Star { get; set; }
    }
}