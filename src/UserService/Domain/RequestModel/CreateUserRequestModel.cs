namespace UserService.Domain.RequestModel
{
    public class CreateUserRequestModel
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public User ToModel()
        {
            return new User
            {
                FirstName = FirstName,
                LastName = LastName,
                Email = Email,
                UserName = UserName,
                Password = BCrypt.Net.BCrypt.HashPassword(Password),
                Type = "user"
            };
        }
    }
}