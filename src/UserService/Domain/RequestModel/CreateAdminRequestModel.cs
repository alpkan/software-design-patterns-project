namespace UserService.Domain.RequestModel
{
    public class CreateAdminRequestModel
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public User ToModel()
        {
            return new User
            {
                UserName = UserName,
                Email = Email,
                Password = BCrypt.Net.BCrypt.HashPassword(Password),
                Type = "admin"
            };
        }
    }
}