using FluentValidation;
using UserService.Domain.RequestModel;
using UserService.Exception;

namespace UserService.Validation
{
    public class CreateAdminValidation : AbstractValidator<CreateAdminRequestModel>
    {
        public CreateAdminValidation() {
            RuleFor(x => x.Email).NotNull().NotEmpty().OnFailure(x => throw new EmailEmptyException());
            RuleFor(x => x.UserName).NotNull().NotEmpty().OnFailure(x => throw new UsernameEmptyException());
            RuleFor(x => x.Password).NotNull().NotEmpty().OnFailure(x => throw new PasswordEmptyException());
        }
    }
}