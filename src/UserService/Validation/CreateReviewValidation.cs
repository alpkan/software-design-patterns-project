using FluentValidation;
using UserService.Domain.RequestModel;
using UserService.Exception;

namespace UserService.Validation
{
    public class CreateReviewValidation : AbstractValidator<CreateReviewRequestModel>
    {
        public CreateReviewValidation()
        {
            RuleFor(x => x.Content).NotNull().NotEmpty().OnFailure(x => throw new ContentEmptyException());
            RuleFor(x => x.Title).NotNull().NotEmpty().OnFailure(x => throw new TitleEmptyException());
            RuleFor(x => x.Star).NotNull().NotEmpty().OnFailure(x => throw new StarEmptyException());
            RuleFor(x => x.Star).GreaterThanOrEqualTo(1).LessThanOrEqualTo(5)
                .OnFailure(x => throw new StarInvalidException(x.Star));
        }
    }
}