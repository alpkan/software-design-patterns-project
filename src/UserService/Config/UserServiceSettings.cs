namespace UserService.Config
{
    public class UserServiceSettings
    {
        public string MongoConnStr { get; set; }
        public string MongoDbName { get; set; }
    }
}