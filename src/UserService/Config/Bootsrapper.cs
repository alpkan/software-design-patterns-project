using Core.Mongo.Context;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using UserService.Repository;
using UserService.Service;

namespace UserService.Config
{
    public static class Bootsrapper
    {
        public static void RegisterComponents(IServiceCollection services)
        {
            var settings = services.BuildServiceProvider().GetService<UserServiceSettings>();

            RegisterRepositories(services, settings);
            RegisterServices(services, settings);
        }

        private static void RegisterRepositories(IServiceCollection services, UserServiceSettings settings)
        {
            var mongoClient = new MongoClient(settings.MongoConnStr);
            var context = new Context(mongoClient, settings.MongoDbName);
            services.AddSingleton<IContext, Context>(provider => context);
            // services.AddSingleton<IUserRepository, UserRepository>();
            services.AddSingleton<IUserRepository, UserRepositoryDecorator>();
            // services.AddSingleton<IReviewRepository, ReviewRepository>();
            services.AddSingleton<IReviewRepository, ReviewRepositoryDecorator>();
        }

        private static void RegisterServices(IServiceCollection services, UserServiceSettings settings)
        {
            services.AddSingleton<IApplicationService, ApplicationService>();
        }
    }
}