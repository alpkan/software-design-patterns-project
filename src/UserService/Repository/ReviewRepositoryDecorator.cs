using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Mongo.Context;
using Microsoft.Extensions.Logging;
using UserService.Domain;

namespace UserService.Repository
{
    public class ReviewRepositoryDecorator : IReviewRepository
    {
        private readonly ILogger<ReviewRepositoryDecorator> _logger;
        private readonly IReviewRepository _reviewRepository;

        public ReviewRepositoryDecorator(ILogger<ReviewRepositoryDecorator> logger, IContext context)
        {
            _logger = logger;
            _reviewRepository = new ReviewRepository(context);
        }
        public async Task<Review> GetReviewById(Guid id)
        {
            return await _reviewRepository.GetReviewById(id);
        }

        public async Task<List<Review>> GetReviewsByUserId(Guid id)
        {
            return await _reviewRepository.GetReviewsByUserId(id);
        }

        public async Task<List<Review>> GetReviews(string status = null)
        {
            return await _reviewRepository.GetReviews(status);
        }

        public async Task<Review> ReplaceReview(Review review)
        {
            return await _reviewRepository.ReplaceReview(review);
        }

        public async Task<Guid> CreateReview(Review review)
        {
            return await _reviewRepository.CreateReview(review);
        }

        public async Task<bool> ApproveReview(Guid id, Guid operatedBy)
        {
            return await _reviewRepository.ApproveReview(id, operatedBy);
        }

        public async Task<bool> RejectReview(Guid id, Guid operatedBy, string reason)
        {
            return await _reviewRepository.RejectReview(id, operatedBy, reason);
        }
    }
}