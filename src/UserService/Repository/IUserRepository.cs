using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UserService.Domain;

namespace UserService.Repository
{
    public interface IUserRepository
    {
        Task<User> GetUserById(Guid id);
        Task<User> GetUserByUsername(string username);
        Task<List<User>> GetUsers();
        Task<Guid> CreateUser(User user);
    }
}