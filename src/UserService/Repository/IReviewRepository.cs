using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UserService.Domain;

namespace UserService.Repository
{
    public interface IReviewRepository
    {
        Task<Review> GetReviewById(Guid id);
        Task<List<Review>> GetReviewsByUserId(Guid id);
        Task<List<Review>> GetReviews(string status = null);
        Task<Review> ReplaceReview(Review review);
        Task<Guid> CreateReview(Review review);
        Task<bool> ApproveReview(Guid id, Guid operatedBy);
        Task<bool> RejectReview(Guid id, Guid operatedBy, string reason);
    }
}