using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Mongo.Context;
using Core.Mongo.Repository;
using UserService.Domain;

namespace UserService.Repository
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(IContext context) : base(context)
        {
        }

        public async Task<User> GetUserById(Guid id)
        {
            return await FindOneAsync(u => u.Id.Equals(id));
        }

        public async Task<User> GetUserByUsername(string username)
        {
            return await FindOneAsync(u => u.UserName.Equals(username));
        }

        public async Task<List<User>> GetUsers()
        {
            return await FindAll();
        }

        public async Task<Guid> CreateUser(User user)
        {
            return (Guid) await CreateOneAsync(user);
        }
    }
}