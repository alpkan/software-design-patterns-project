using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Core.Mongo.Context;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using UserService.Domain;

namespace UserService.Repository
{
    public class UserRepositoryDecorator : IUserRepository
    {
        private readonly ILogger<UserRepositoryDecorator> _logger;
        private readonly IUserRepository _userRepository;

        public UserRepositoryDecorator(ILogger<UserRepositoryDecorator> logger, IContext context)
        {
            _logger = logger;
            _userRepository = new UserRepository(context);
        }

        public async Task<User> GetUserById(Guid id)
        {
            _logger.LogInformation($"Get user with id requested. value: {id}");
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var res = await _userRepository.GetUserById(id);
            stopwatch.Stop();
            _logger.LogInformation($"Took: {stopwatch.Elapsed} - Returned result: {JsonConvert.SerializeObject(res)}");
            return res;
        }

        public async Task<User> GetUserByUsername(string username)
        {
            _logger.LogInformation($"Get user with username requested. value: {username}");
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var res = await _userRepository.GetUserByUsername(username);
            stopwatch.Stop();
            _logger.LogInformation($"Took: {stopwatch.Elapsed} - Returned result: {JsonConvert.SerializeObject(res)}");
            return res;
        }

        public async Task<List<User>> GetUsers()
        {
            _logger.LogInformation($"Get users");
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var res = await _userRepository.GetUsers();
            stopwatch.Stop();
            _logger.LogInformation($"Took: {stopwatch.Elapsed} - Returned result: {JsonConvert.SerializeObject(res)}");
            return res;
        }

        public async Task<Guid> CreateUser(User user)
        {
            _logger.LogInformation($"Create user requested. value: {JsonConvert.SerializeObject(user)}");
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var res = await _userRepository.CreateUser(user);
            stopwatch.Stop();
            _logger.LogInformation($"Took: {stopwatch.Elapsed} - Returned result: {JsonConvert.SerializeObject(res)}");
            return res;
        }
    }
}