using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Mongo.Context;
using Core.Mongo.Repository;
using UserService.Domain;
using UserService.Exception;

namespace UserService.Repository
{
    public class ReviewRepository : Repository<Review>, IReviewRepository
    {
        public ReviewRepository(IContext context) : base(context)
        {
        }

        public async Task<Review> GetReviewById(Guid id)
        {
            return await FindOneAsync(u => u.Id.Equals(id));
        }
        
        public async Task<List<Review>> GetReviewsByUserId(Guid id)
        {
            return await FindManyAsync(u => u.UserId.Equals(id));
        }
        
        public async Task<List<Review>> GetReviews(string status = null)
        {
            return await FindManyAsync(u => u.Status.Equals("approved") || status == null);
        }

        public async Task<List<Review>> GetReviews()
        {
            return await FindAll();
        }
        
        public async Task<Review> ReplaceReview(Review review)
        {
            var result = await ReplaceOneAsync(p => p.Id.Equals(review.Id) && p.UserId.Equals(review.UserId), review);
            if (result > 0)
                return review;
            throw new ReviewNotFound(review.Id.ToString());
        }
        
        public async Task<Guid> CreateReview(Review review)
        {
            return (Guid) await CreateOneAsync(review);
        }
        
        public async Task<bool> ApproveReview(Guid id, Guid operatedBy)
        {
            var result = await UpdateOneAsync(p => p.Id.Equals(id), (r => r.Status, "approved"), (r => r.OperatedBy, operatedBy));
            return result > 0;
        }
        
        public async Task<bool> RejectReview(Guid id, Guid operatedBy, string reason)
        {
            var result = await UpdateOneAsync(p => p.Id.Equals(id), (r => r.Status, "rejected"), (r => r.OperatedBy, operatedBy), (r => r.RejectReason, reason));
            return result > 0;
        }
    }
}