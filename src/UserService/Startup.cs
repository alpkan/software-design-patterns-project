using Core.Middlewares.Error;
using Core.Middlewares.Logging;
using Core.Swagger;
using Core.Validation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using UserService.Config;

namespace UserService
{
    public class Startup
    {
        private readonly UserServiceSettings _settings;
        public Startup(IWebHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
            _settings = Configuration.GetSection("ApiSettings").Get<UserServiceSettings>();
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(_settings);
            
            services.AddSingleton(typeof(ILogger<>), typeof(Logger<>));
            
            Bootsrapper.RegisterComponents(services);
            
            services.AddTSwaggerGen("User Service", "User service for solution partner.");

            services.AddTValidation<Startup>();
            
            services.AddResponseCompression();

            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<ErrorHandlingMiddleware>();
            app.UseMiddleware<ErrorLoggingMiddleware>("UserService");
            
            app.UseTSwaggerUI("User Service");

            app.UseResponseCompression();
            
            app.UseRouting();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}