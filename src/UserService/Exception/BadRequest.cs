using Core.Middlewares.Error;

namespace UserService.Exception
{
    public class IdInvalidException : BadRequest
    {
        public IdInvalidException(string id) : base(4001, "id", id){}
    }

    public class EmailEmptyException : BadRequest
    {
        public EmailEmptyException() : base(4002, message: "Email can not be null or empty"){}
    }

    public class UsernameEmptyException : BadRequest
    {
        public UsernameEmptyException() : base(4003, message: "Username can not be null or empty"){}
    }

    public class PasswordEmptyException : BadRequest
    {
        public PasswordEmptyException() : base(4004, message: "Password can not be null or empty"){}
    }
    public class ContentEmptyException : BadRequest
    {
        public ContentEmptyException() : base(4005, message: "Content can not be null or empty"){}
    }
    public class StarInvalidException : BadRequest
    {
        public StarInvalidException(int star) : base(4006, message: $"Star should between 1-5: {star}"){}
    }

    public class TitleEmptyException : BadRequest
    {
        public TitleEmptyException() : base(4007, message: "Title can not be null or empty"){}
    }

    public class StarEmptyException : BadRequest
    {
        public StarEmptyException() : base(4008, message: "Star can not be null or empty"){}
    }

    public class FirstnameEmptyException : BadRequest
    {
        public FirstnameEmptyException() : base(4009, message: "Firstname can not be null or empty"){}
    }

    public class LastnameEmptyException : BadRequest
    {
        public LastnameEmptyException() : base(40010, message: "Lastname can not be null or empty"){}
    }
}