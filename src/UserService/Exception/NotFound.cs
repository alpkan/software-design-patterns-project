using Core.Middlewares.Error;

namespace UserService.Exception
{
    public class UserNotFound : NotFound
    {
        public UserNotFound(string id) : base(4041, $"User not found with id: {id}"){}
        public UserNotFound() : base(4041, $"User not found"){}
    }
    
    public class ReviewNotFound : NotFound
    {
        public ReviewNotFound(string id) : base(4041, $"Review not found with id: {id}"){}
    }
}