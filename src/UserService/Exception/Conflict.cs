using Core.Middlewares.Error;

namespace UserService.Exception
{
    public class AlreadyExists : Conflict
    {
        public AlreadyExists(string key) : base(4091, message: $"Item already exist with: {key}"){}
    }
}