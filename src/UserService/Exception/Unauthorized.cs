using Core.Middlewares.Error;

namespace UserService.Exception
{
    public class UserUnauthorized : Unauthorized
    {
        public UserUnauthorized() : base(4041){}
    }
}